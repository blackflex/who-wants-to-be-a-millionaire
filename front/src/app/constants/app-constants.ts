export const API_URL = 'http://127.0.0.1:8000';

export const DEFAULT_ERROR_MESSAGE = 'Something went wrong,please try again later.';

export const ACCESS_TOKEN = 'access_token';

export const REFRESH_TOKEN = 'refresh_token';
