import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth/auth.service';
import {RequestHelperService} from '../../../services/request-helper/request-helper.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(
    private authService: AuthService,
    public requestHelper: RequestHelperService
  ) { }

  ngOnInit(): void {
  }

  login() {
     if (this.loginForm.valid) {
       this.authService.login(this.loginForm.value);
     }
  }

}
