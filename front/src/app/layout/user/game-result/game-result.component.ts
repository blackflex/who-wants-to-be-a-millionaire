import { Component, OnInit } from '@angular/core';
import {GameService} from '../../../services/game/game.service';
import {RequestHelperService} from '../../../services/request-helper/request-helper.service';
import {AuthService} from '../../../services/auth/auth.service';

@Component({
  selector: 'app-game-result',
  templateUrl: './game-result.component.html',
  styleUrls: ['./game-result.component.scss'],
  providers: [GameService]
})
export class GameResultComponent implements OnInit {

  constructor(
    public gameService: GameService,
    public requestHelper: RequestHelperService,
    public authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.gameService.getResults();
  }

}
