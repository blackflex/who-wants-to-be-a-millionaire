import { Component, OnInit } from '@angular/core';
import {GameService} from '../../../services/game/game.service';
import {RequestHelperService} from '../../../services/request-helper/request-helper.service';
import {Result} from '../../../interfaces/result';
import {Answer} from '../../../interfaces/answer';
import {Question} from '../../../interfaces/question';

@Component({
  selector: 'app-start-game',
  templateUrl: './start-game.component.html',
  styleUrls: ['./start-game.component.scss'],
  providers: [GameService]
})
export class StartGameComponent implements OnInit {

  result: Result[] = [];

  constructor(
    public gameService: GameService,
    public requestHelper: RequestHelperService
  ) { }

  ngOnInit(): void {
    this.gameService.startGame();
  }

  confirm(question: Question, answer: Answer, qIndex, aIndex) {
    if (!answer.answered) {
      const questionId = question.id;
      const answerId = answer.id;
      const questionIndex = this.result.findIndex(item => item.questionId === questionId);
      if (questionIndex === -1) {
        this.result.push({
          questionId: question.id,
          answersIds: [answerId]
        });
      } else {
        this.result[questionIndex].answersIds.push(answerId);
      }
      this.gameService.questions[qIndex].answers[aIndex].answered = true;
    }
  }

  finish() {
   this.gameService.saveResult(this.result);
   this.result = [];
  }

}
