import {User} from './user';

export interface Scores {
  score: number;
  user: User;
}
