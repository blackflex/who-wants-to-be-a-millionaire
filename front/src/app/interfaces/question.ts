import {Answer} from './answer';

export interface Question {
  id: number;
  title: string;
  weight: number;
  answers: Answer[];
}
