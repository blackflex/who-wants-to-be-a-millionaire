export interface Result {
  questionId: number;
  answersIds: [number];
}
