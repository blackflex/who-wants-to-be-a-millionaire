export interface Answer {
  id: number;
  title: string;
  isRight: string;
  answered: boolean;
}
