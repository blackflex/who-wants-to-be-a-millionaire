import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_URL} from '../../constants/app-constants';
import {RequestHelperService} from '../request-helper/request-helper.service';
import {SnackbarService} from '../snackbar/snackbar.service';
import {Question} from '../../interfaces/question';
import {Scores} from '../../interfaces/scores';
import {Result} from '../../interfaces/result';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  questions: Question[] = [];
  scores: Scores[] = [];

  constructor(
    public http: HttpClient,
    private requestHelper: RequestHelperService,
    private snackbarService: SnackbarService
  ) { }

  startGame() {
    this.questions = [];
    this.requestHelper.loading = true;
    this.http.get<Question[]>(`${API_URL}/questions`, this.requestHelper.appendHeader()).subscribe(data => {
      if (data) {
        this.questions = data;
      } else {
        this.snackbarService.openSnackBar();
      }
      this.requestHelper.loading = false;
    }, this.requestHelper.handleError);
  }

  getResults() {
    this.requestHelper.loading = true;
    this.http.get<Scores[]>(`${API_URL}/results/top`, this.requestHelper.appendHeader()).subscribe(data => {
      if (data) {
        this.scores = data;
      } else {
        this.snackbarService.openSnackBar();
      }
      this.requestHelper.loading = false;
    }, this.requestHelper.handleError);
  }

  saveResult(result) {
    this.requestHelper.loading = true;
    const body = {
      answered: result,
      questionsIds: result.map(item => item.questionId)
    };
    this.http.post<{success: boolean, score: number}>(
      `${API_URL}/results/save/`,
          body,
          this.requestHelper.appendHeader()
      ).subscribe(data => {
        if (data.success) {
          this.snackbarService.openSnackBar(
            `Your score is ${data.score.toFixed(3)}`,
            (data.score) ? 'success' : 'error'
          );
        } else {
          this.snackbarService.openSnackBar();
        }
        this.startGame();
        this.requestHelper.loading = false;
    }, this.requestHelper.handleError);
  }
}
