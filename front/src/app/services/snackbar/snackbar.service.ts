import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackbarComponent} from '../../layout/parts/snackbar/snackbar.component';
import {DEFAULT_ERROR_MESSAGE} from '../../constants/app-constants';



@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(public snackbar: MatSnackBar) {}

  openSnackBar(message = DEFAULT_ERROR_MESSAGE, className = 'error') {
    this.snackbar.openFromComponent(SnackbarComponent, {
      duration: 4500,
      data: message,
      verticalPosition: 'top',
      panelClass: 'snackbar-' + className
    });
  }

}
