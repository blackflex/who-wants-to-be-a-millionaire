import { Injectable } from '@angular/core';
import {HttpHeaders} from '@angular/common/http';

import {SnackbarService} from '../snackbar/snackbar.service';
import {ACCESS_TOKEN} from '../../constants/app-constants';

@Injectable({
  providedIn: 'root'
})
export class RequestHelperService {

  loading = false;

  constructor(
    private snackbarService: SnackbarService
  ) { }


  handleError = (err) => {
    console.log('err', err)
    this.loading = false;
    this.snackbarService.openSnackBar(err.error?.detail || err.message);
  }

  appendHeader() {
    return {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`
      }),
    };
  }
}
