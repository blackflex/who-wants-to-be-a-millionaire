import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ACCESS_TOKEN, API_URL, REFRESH_TOKEN} from '../../constants/app-constants';
import {Router} from '@angular/router';
import {RequestHelperService} from '../request-helper/request-helper.service';
import {SnackbarService} from '../snackbar/snackbar.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import {Token} from '../../interfaces/token';
import {User} from '../../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userId: number;
  helper = new JwtHelperService();

  constructor(
    public http: HttpClient,
    private router: Router,
    private requestHelper: RequestHelperService,
    private snackbarService: SnackbarService
  ) { }


  login(body) {
    this.requestHelper.loading = true;
    this.http.post<Token>(`${API_URL}/auth/token/`, body).subscribe( data => {
      if (this.storeToken(data)) {
        this.router.navigate(['game']);
      }
      this.requestHelper.loading = false;
    }, this.requestHelper.handleError);
  }

  register(body) {
    this.requestHelper.loading = true;
    this.http.post<{success: boolean}>(`${API_URL}/users/register/`, body).subscribe( data => {
      if (data.success) {
        this.snackbarService.openSnackBar('User registered', 'success');
        this.login(body);
      }
      this.requestHelper.loading = false;
    }, this.requestHelper.handleError);
  }

  storeToken(data: Token) {
    const accessToken = data.access;
    const refreshToken = data.refresh;
    if (accessToken) {
      localStorage.setItem(ACCESS_TOKEN, accessToken);
      if (refreshToken) {
        localStorage.setItem(REFRESH_TOKEN, refreshToken);
      }
      return true;
    } else {
      this.snackbarService.openSnackBar();
    }
    return false;
  }

  checkIfTokenValid() {
    this.userId = null;
    const token = localStorage.getItem(ACCESS_TOKEN);
    if (token) {
      const decodedToken = this.helper.decodeToken(token);
      const isExpired = this.helper.isTokenExpired(token);
      if (!isExpired) {
        this.userId = decodedToken.user_id;
      }
    }
    return !!this.userId;
  }

  async isAuth() {
    const cond = await this.checkIfAuth();
    if (!cond) {
      this.router.navigate(['login']);
    }
    return cond;
  }

  async isAnonymous() {
    const cond = await this.checkIfAuth();
    if (cond) {
      this.router.navigate(['game']);
    }
    return !cond;
  }

  async checkIfAuth() {
    try {
      if (!this.checkIfTokenValid()) {
        const refreshedToken = await this.refreshToken();
        if (refreshedToken) {
          this.checkIfTokenValid();
        }
      }
    } catch (e) {
      this.snackbarService.openSnackBar('NOT VALID JWT TOKEN');
    }
    return !!this.userId;
  }

  refreshToken() {
    return new Promise(resolve => {
      const refresh = localStorage.getItem(REFRESH_TOKEN);
      if (refresh) {
        this.http.post<Token>(`${API_URL}/auth/token/refresh/`, {refresh}).subscribe( data => {
          resolve(this.storeToken(data));
        });
      } else {
        resolve(false);
      }
    });
  }

  logout() {
    localStorage.removeItem(REFRESH_TOKEN);
    localStorage.removeItem(ACCESS_TOKEN);
    this.router.navigate(['login']);
  }

}
