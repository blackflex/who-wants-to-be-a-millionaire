import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './layout/auth/login/login.component';
import { StartGameComponent } from './layout/user/start-game/start-game.component';
import {MaterialModule} from './material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { RegisterComponent } from './layout/auth/register/register.component';
import { MenuComponent } from './layout/parts/menu/menu.component';
import { GameWrapperComponent } from './layout/user/game-wrapper/game-wrapper.component';
import { GameResultComponent } from './layout/user/game-result/game-result.component';
import { SnackbarComponent } from './layout/parts/snackbar/snackbar.component';
import { LoaderComponent } from './layout/loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    StartGameComponent,
    RegisterComponent,
    MenuComponent,
    GameWrapperComponent,
    GameResultComponent,
    SnackbarComponent,
    LoaderComponent
  ],
  imports: [
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  entryComponents: [
    SnackbarComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
