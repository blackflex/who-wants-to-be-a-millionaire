import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './layout/auth/login/login.component';
import {AuthGuard} from './guards/auth/auth.guard';
import {StartGameComponent} from './layout/user/start-game/start-game.component';
import {RegisterComponent} from './layout/auth/register/register.component';
import {GameWrapperComponent} from './layout/user/game-wrapper/game-wrapper.component';
import {GameResultComponent} from './layout/user/game-result/game-result.component';
import {AnonymousGuard} from './guards/anonymous/anonymous.guard';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'game',
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AnonymousGuard],
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [AnonymousGuard],
  },
  {
   path: 'game',
   component: GameWrapperComponent,
   canActivate: [AuthGuard],
   children: [
     {
       path: 'play',
       component: StartGameComponent,
       canActivate: [AuthGuard]
     },
     {
       path: 'results',
       component: GameResultComponent,
       canActivate: [AuthGuard],
     }
   ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
