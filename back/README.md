# **Before to start**

###### **SUPER USER**

**Login: admin**

**Password: 77admin77**

## setup
1) ### `pipenv install`

## create super user 
1) ### `cd millioner_api`
2) ### `python manage.py createsuperuser`

## run
1) ### `cd millioner_api`
2) ### `python manage.py runserver`

## test
1) ### `cd millioner_api`
2) ### `python manage.py test`

