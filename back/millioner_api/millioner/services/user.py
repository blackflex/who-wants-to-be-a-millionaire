import json
from django.contrib.auth.models import User
from rest_framework import status


def register(body):
    content = {'detail': 'Validation Error'}
    try:
        body = json.loads(body.decode())
        username = body['username']
        email = body['email']
        password = body['password']
        first_name = body['firstName']
        last_name = body['lastName']
        if username and email and first_name and last_name and password:
            user = User.objects.create_user(
                username,
                email,
                password
            )
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            content = {'success': True}
            return {
                'content': content,
                'status': status.HTTP_201_CREATED
            }
        else:
            return {
                'content': content,
                'status': status.HTTP_400_BAD_REQUEST
            }
    except:
        return {
            'content': content,
            'status': status.HTTP_400_BAD_REQUEST
        }