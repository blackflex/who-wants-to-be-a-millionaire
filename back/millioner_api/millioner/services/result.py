from rest_framework import status
from ..models import Result, Question
from django.contrib.auth.models import User
import json


def top_result():
    results = Result.objects.raw('''
                SELECT r.id as resId, au.*, MAX(r.score) as score
                FROM millioner_result r
                JOIN auth_user au on r.user_id = au.id
                GROUP BY r.user_id
                ORDER BY score DESC, id DESC
                LIMIT 10 
                ''')
    top_ten = []
    for result in results:
        top_ten.append({
            'score': result.score,
            'user': {
                'id': result.id,
                'username': result.username,
                'first_name': result.first_name,
                'last_name': result.last_name,
            }
        })
    return top_ten


def save_result(body, userId):
    content = {'detail': 'Invalid parameters'}
    try:
        body = json.loads(body.decode())
        user = User.objects.get(id=userId)
        if user:
            answered = body['answered']
            questions_ids = body['questionsIds']
            total_score = 0
            questions = Question.objects.filter(pk__in=questions_ids)
            for answer in answered:
                question = next((item for item in questions if item.id == answer['questionId']), None)
                if question:
                    right_answers = set(map(lambda a: a.id, question.answers.filter(isRight=True)))
                    one_right_answer_weight = question.weight / len(right_answers)
                    user_answers = set(answer['answersIds'])
                    user_right_answers = list(right_answers.intersection(user_answers))
                    user_wrong_answers = list(user_answers.difference(right_answers))

                    answer_score = (one_right_answer_weight * len(user_right_answers)) - (one_right_answer_weight * len(user_wrong_answers))
                    if answer_score > 0:
                        total_score += answer_score

            Result.objects.create(score=total_score, user=user)
            content = {'success': True, 'score': total_score}
            return {
                'content': content,
                'status': status.HTTP_201_CREATED
            }
    except:
        return {
            'content': content,
            'status': status.HTTP_400_BAD_REQUEST
        }

    return {
        'content': content,
        'status': status.HTTP_400_BAD_REQUEST
    }