from rest_framework import viewsets, permissions
from .models import Question, Answer, Result
from .services.result import save_result, top_result
from .services.user import register
from django.contrib.auth.models import User
from .serializers import UserSerializer, QuestionSerializer, AnswerSerializer, ResultSerializer
from rest_framework.decorators import action
from rest_framework.response import Response


class UsersView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    @action(detail=False, methods=['post'], permission_classes=(permissions.AllowAny,))
    def register(self, request):
        response = register(request.body)
        return Response(response['content'], response['status'])


class QuestionView(viewsets.ModelViewSet):
    queryset = Question.objects.order_by('?')[:5]
    serializer_class = QuestionSerializer


class AnswersView(viewsets.ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer


class ResultView(viewsets.ModelViewSet):
    queryset = Result.objects.all()
    serializer_class = ResultSerializer

    @action(detail=False, methods=['get'])
    def top(self, request):
        top_results = top_result()
        return Response(top_results)

    @action(detail=False, methods=['post'])
    def save(self, request):
        response = save_result(request.body, request.user.id)
        return Response(response['content'], response['status'])
