# Generated by Django 3.0.5 on 2020-04-23 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('millioner', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answer',
            name='question',
        ),
        migrations.AddField(
            model_name='question',
            name='answers',
            field=models.ManyToManyField(to='millioner.Answer'),
        ),
    ]
