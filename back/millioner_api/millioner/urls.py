from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('users', views.UsersView)
router.register('questions', views.QuestionView)
router.register('answers', views.AnswersView)
router.register('results', views.ResultView)
urlpatterns = [
   path('', include(router.urls))
]