from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User


class Answer(models.Model):
    title = models.CharField(max_length=255)
    isRight = models.BooleanField()
    def __str__(self):
        return self.title


class Question(models.Model):
    title = models.CharField(max_length=255)
    weight = models.IntegerField(validators=[
        MaxValueValidator(20),
        MinValueValidator(5)
    ])
    answers = models.ManyToManyField(Answer)
    def __str__(self):
        return self.title


class Result (models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    score = models.FloatField()
