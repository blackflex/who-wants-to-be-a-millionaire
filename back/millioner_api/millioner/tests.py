from django.test import TestCase
from rest_framework.test import APITestCase
from rest_framework import status
from django.contrib.auth.models import User
import json


class JobsAPITestCase(APITestCase):

    test_user = {
        'username': 'test555',
        'email': 'test555@gmail.com',
        'password': '77test77',
        'firstName': 'test',
        'lastName': 'test',
    }

    # user registration, must validations fail, no body
    def test_register_user_fail_no_body(self):
        url = 'http://127.0.0.1:8000/users/register/'
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # user registration,must validations fail ,because password is required
    def test_user_register_fail_password(self):
        url = 'http://127.0.0.1:8000/users/register/'
        # removing password field
        user = self.test_user.copy().pop('password')
        response = self.client.post(url, data=json.dumps(user), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # user registration,success case
    def test_user_register_success(self):
        url = 'http://127.0.0.1:8000/users/register/'
        user = self.test_user
        response = self.client.post(url, data=json.dumps(user), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    # unauthorized users cannot get game questions
    def test_get_game_questions_unauthorized(self):
        url = 'http://127.0.0.1:8000/questions/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    # authorized users can get game questions
    def test_get_game_questions_success(self):
        url = 'http://127.0.0.1:8000/questions/'
        user = User.objects.create_user(
            'test',
            'test@gmail.com',
            '12345'
        )
        self.client.force_authenticate(user=user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)